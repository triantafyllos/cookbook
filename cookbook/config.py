import os


class Config(object):
    """
    List of environment variables used by the app.
    """
    DATABASE_URI = os.environ.get('DATABASE_URI', 'postgres://user:pass@postgres/cookbook')
