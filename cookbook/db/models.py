from sqlalchemy import MetaData, Column, Integer, Text, Table


metadata = MetaData()

recipes = Table(
    'recipes',
    metadata,
    Column('page_no', Integer, unique=True),
    Column('title', Text)
)
