

FETCH_COOKBOOK = '''
    WITH cookbook AS (
        SELECT
            generate_series(0, max(page_no), 2) AS page
        FROM
            recipes
    )
    
    SELECT
        lr.title AS left_title,
        rr.title AS right_title
    FROM
        cookbook AS c
    LEFT JOIN recipes AS lr
        ON c.page=lr.page_no
    LEFT JOIN recipes AS rr
        ON c.page=rr.page_no-1
'''
