import pandas as pd
import click
from tabulate import tabulate

from cookbook.db.models import recipes
from cookbook.db.queries import FETCH_COOKBOOK
from cookbook.factory import initialise_db

engine = initialise_db()


@click.group()
def cli():
    pass


@cli.command()
@click.option('-d', '--drop-first', is_flag=True, help="Drop tables before creating.")
def migrate(drop_first):
    """
    Drops (optionally) and creates recipes table into the database.
    """
    if drop_first:
        recipes.drop(engine, checkfirst=True)
    recipes.create(engine)


@cli.command()
@click.argument('dataset-file', type=click.File('rb'))
def insert(dataset_file):
    """
    Bulk insert data into recipes table using Postgres COPY command.
    """
    conn = engine.raw_connection()
    try:
        with conn.cursor() as cur:
            cur.copy_from(dataset_file, 'recipes', sep=',')
        conn.commit()
    except:
        conn.rollback()
        raise
    finally:
        conn.close()


@cli.command()
def run():
    """
    Generates and prints the cookbook.
    """
    data = pd.read_sql(FETCH_COOKBOOK, engine)
    print(tabulate(data, headers='keys', tablefmt='psql', showindex=False))
