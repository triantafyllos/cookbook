from sqlalchemy import create_engine
from cookbook.config import Config


def initialise_db():
    """
    Database engine initialization.
    """
    engine = create_engine(Config.DATABASE_URI)
    return engine
