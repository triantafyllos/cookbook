# Cookbook

Given the definition of the problem, I generated a query that meets the specifications. 
Here are the steps that I followed:

1) First of all we need all the pages of the cookbook 
and not just the ones that exist in recipes table,
so I used Postgres `generate_series` function (i could also use a custom solution but I preferred to used a native one from postgres).

2) At `generate_series` function I used the range `[0, max_page_number]` from recipes table.
The tricky part is that we need to generate pages with step=2, so as to have the following sequence: 
0, 2, 4, 6, 8.. That sequence is the pointers for every left page of the book.

3) Then since every cookbook page has only one recipe, we left join the above sequence with the recipes tables on `page_no`.
As a result, we will have all the left pages of the cookbook. Using left join, we will keep the first page of the cookbook 
(which by definition is empty).

4) We also need the right side of the pages. So, we left join the initial sequence with the `page_no-1`.
As a result, right pages like {1,3,5} will match with sequence values {0,2,4}.


## Installation

First we create and activate the environment:
```
virtualenv -p python3 env
. env/bin/activate
```

Then, we install the requirements as well the CLI tool:
```
pip install -e .
```

## Execution 

Before running any command, we need to set the DB URL into the environment
```
export DATABASE_URI=postgres://user:pass@postgres/cookbook
```
 
There is available a CLI tool that:
- drops/creates the recipes table, 
- inserts a batch of data into DB
- runs the required query from the assessment  

Specifically, in order to drop/create the cookbook table we use the following command:
```
cookbookcli migrate --drop-first
```
Then, we can add recipes from CSV files into the database. 
There are available the 2 cases that are described in the assessment.
- data/case1.csv
- data/case2.csv
```
cookbookcli insert data/case1.csv
```

Finally, we run the SQL query and print the results into the stdout.
```
cookbookcli run
```

## Dockerization (Installation & Execution)

First we need to install docker-compose cli tool.
```
pip install docker-compose
```

Then, we build the docker image for our cookbook project.
```
docker build -t cookbook:0.0.1 .
```
Now, we start both our cookbook project as well as a dockerized PostgresSQL using docker-compose.
Cookbook project waits until Postgres is up-and-running.
```
docker-compose up
```

Finally we run dockerized the same commands as described in Execution section.
The previous command generates a new container for cookbook project with the name `cookbook`,
so we can generate the cookbook with the following commands.

```
docker exec -it cookbook cookbookcli migrate --drop-first
docker exec -it cookbook cookbookcli insert data/case2.csv
docker exec -it cookbook cookbookcli run
```
