from setuptools import setup, find_packages

setup(
    name='cookbook',
    version='0.0.1',
    description='Creates a cookbook using recipes.',
    install_requires=[r.replace('\n', '') for r in open('requirements.txt')],
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'cookbookcli = cookbook.cli:cli'
        ]
    }
)
